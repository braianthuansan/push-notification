importScripts('https://www.gstatic.com/firebasejs/3.5.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.5.0/firebase-messaging.js');

const firebaseConfig = {
    apiKey: "AIzaSyDPoqDK4s4F0GiAE8Q4DzmofUX2NOZz3gQ",
    authDomain: "cinndet-firebase.firebaseapp.com",
    projectId: "cinndet-firebase",
    storageBucket: "cinndet-firebase.appspot.com",
    messagingSenderId: "223066475833",
    appId: "1:223066475833:web:f8aa74360d940ba38bace8",
    measurementId: "G-NZDP7FWWDJ"
  };
  firebase.initializeApp(firebaseConfig);
  const messaging = firebase.messaging();
  messaging.setBackgroundMessageHandler(function(payload){
      const promiseChain = clients
      .matchAll({
          type: "window",
          includeUncontrolled: true,
      })
      .then((windowClients)=>{
          for (Let i = 0; i < windowClients.length; i++){
              const windowClient = windowClients[i];
              windowClient.postMessage(payload);
          }
      })
      .then(()=>{
          return registration.showNotification("my notification tittle")
      });
      return promiseChain;
  });
  self.addEventListener("notificationclick", function(event){
      console.log(event);
  });