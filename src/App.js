
import {BrowserRouter, Switch, Route, Redirect} from'react-router-dom'
import { Header } from './Component/Header/Header';
import { Contact } from './Page/Contact';
import { Home } from './Page/Home';
import React from 'react';

import { getTokenFirebase } from './firebase/firebase';

getTokenFirebase()

export const App =() => {
  
  return (
   <BrowserRouter>
   <Header/>
    <Switch>
      <Route exact path='/' component={Home}/>
      <Route exact path='/contact' component={Contact}/>
      <Redirect to='/'/>
    </Switch>
   </BrowserRouter>
  );
}
