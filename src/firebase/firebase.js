import { initializeApp } from "firebase/app";
import { getMessaging, getToken, onMessage } from "firebase/messaging";

const firebaseConfig = {
  apiKey: "AIzaSyDPoqDK4s4F0GiAE8Q4DzmofUX2NOZz3gQ",
  authDomain: "cinndet-firebase.firebaseapp.com",
  projectId: "cinndet-firebase",
  storageBucket: "cinndet-firebase.appspot.com",
  messagingSenderId: "223066475833",
  appId: "1:223066475833:web:f8aa74360d940ba38bace8",
  measurementId: "G-NZDP7FWWDJ"
};
// Get Messaging
const messaging = getMessaging(firebaseConfig)

const {REACT_APP_VAPID_KEY} = process.env
const publicKey = REACT_APP_VAPID_KEY



// Get Token
export const getTokenFirebase = async()=>{
  let currentToken = ''

  try {
      currentToken = await getToken(messaging, {vapidKey:publicKey})
      if(currentToken){
          console.warn('success token',currentToken)
      } else {
          console.warn('failed token',currentToken)
      }
  } catch (error) {
      console.log('Ocurrio un error obteniendo el token', error)
  }
  return currentToken
}
// On Message
export const firstPlaneMessage = onMessage(messaging, (payload)=>{
  console.log('Message received. ', payload)
})
export const onMessageListener = () =>
  new Promise((resolve) => {
  messaging.onMessage((payload) => {
  resolve(payload);
  });
});
export const Firebaseapp = initializeApp(firebaseConfig);