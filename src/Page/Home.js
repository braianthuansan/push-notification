import { useState } from 'react';
import { Container } from "../Component/Home/Container"
import { Text } from "../Component/Home/Text"
import styled from 'styled-components'

const Button = styled.button`
    border: none;
    border-radius: 2em;
    background-color: darkmagenta;
    cursor: pointer;
`
export const Home= () => {
    const [count, setCount] = useState(0)
    return (
        <Container>
            <Text 
            text={count}
            color='red'
            fontSize='3em'/>
            <Button onClick={()=> setCount(count+1)}>
            <Text 
            text='Click me'
            color='white'
            fontSize='5em'/>
            </Button>
        </Container>
    )
}