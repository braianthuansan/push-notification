import styled from 'styled-components'

const ContainerComponent = styled.div`
    background-color: bisque;
    height: 100vh;
    display: grid;
    place-content: center;
`
export const Container = (props) =>{
    return(
        <ContainerComponent>
            {props.children}

        </ContainerComponent>

    )
}