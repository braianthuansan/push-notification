import styled from 'styled-components'

const TextComponent = styled.h2`
    color: ${props => props.color};
    font-size: ${props => props.fontSize};
`

export const Text = (props)=>{
    return(
        <TextComponent 
        color={props.color}
        fontSize={props.fontSize}>
        {props.text}

    </TextComponent>
    )
    
}
