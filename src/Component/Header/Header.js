import styled from "styled-components"
import {Link} from'react-router-dom'

const HeaderContainerComponent = styled.div`
    width: 1280px;
    height: 10vh;
    display: grid;
    align-content: center;
    justify-content: center;
    grid-template-columns: repeat(2, 1fr);
    background-color: #EFF1F3;
    margin: 0 auto;
`
export const Header = ()=>{
    return(
        <HeaderContainerComponent>
           <Link to='/'>Home</Link>
           <Link to='/contact'>Contact</Link>
        </HeaderContainerComponent>

      
    )
    
}
